import React, { lazy } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={lazy(() => import('pages/Home'))} />
    </Switch>
  );
}

export default withRouter(App);
