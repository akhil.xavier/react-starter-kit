export const DEFAULT_ACTION = 'default/DEFAULT_ACTION';

export const defaultAction = () => ({
  type: DEFAULT_ACTION,
});
