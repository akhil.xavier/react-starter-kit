import { takeEvery } from 'redux-saga/effects';
import { DEFAULT_ACTION } from './action';

export const defaultSaga = function* () {
  try {
    yield;
  } catch (err) {
    yield;
  }
};

const actionWatcher = function* () {
  yield takeEvery(DEFAULT_ACTION, defaultSaga);
};

export default actionWatcher;
