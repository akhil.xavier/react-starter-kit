import produce from 'immer';
import { DEFAULT_ACTION } from './action';

const initialState = {
  default: null,
};

const reducer = (state = initialState, action) => {
  return produce(state, (draft) => {
    const draftState = { ...draft };
    switch (action.type) {
      case DEFAULT_ACTION:
        draftState.default = 1;
        return draftState;
      default:
        return draftState;
    }
  });
};
export default reducer;
